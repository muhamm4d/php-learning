# PHP Learning and Practising
PHP revisions and learning how to use github

This repo is for practising different PHP tricks and tips.

Lessons covered are from Laracasts: https://laracasts.com/series/php-for-beginners
and different resources

#Aims
  To improve the way I usually code.
  To create reusable codes / scripts.
  To create some personal frameworks.
  To collaborate on an open source project
  
#collaborations
So far no need for collaboration but if anyone intends to collaborate and guide me a better way to code,
or just shared with me different resources to learn more about PHP and other languages or an open source project that invlove PHP,
or just wanted to collaborate, that will be really appreciated.
