<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'App\\Controllers\\PagesController' => $baseDir . '/app/controllers/PagesController.php',
    'App\\Controllers\\TaskController' => $baseDir . '/app/controllers/TaskController.php',
    'App\\Core\\App' => $baseDir . '/core/App.php',
    'App\\Core\\Request' => $baseDir . '/core/Request.php',
    'App\\Core\\Router' => $baseDir . '/core/Router.php',
    'App\\Models\\Project' => $baseDir . '/app/models/project.php',
    'ComposerAutoloaderInit35803a8c3499bce2d3eadef38ee6a7d5' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInit35803a8c3499bce2d3eadef38ee6a7d5' => $vendorDir . '/composer/autoload_static.php',
    'Connection' => $baseDir . '/core/database/Connection.php',
    'QueryBuilder' => $baseDir . '/core/database/QueryBuilder.php',
    'Task' => $baseDir . '/core/task.php',
);
