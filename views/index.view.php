<?php require 'partials/head.php'; ?>

		<!-- Perform the task details listing -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<ol>
					<?php foreach($tasks as $task): ?>
						<?php if ($task->completed): ?>
							<li><?= $task->description; ?> &nbsp; &#9989;</li>
						<?php else: ?>
							<li><?= $task->description; ?></li>
						<?php endif ?>
					<?php endforeach ?>
				</ol>
			</div>
		</div>
		<!-- form submission -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2>Form Practising</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<form method="POST" action="/names">
					<input type="text" name="name" class="form-control">
					<br>
					<button type="submit" class="btn-info">Submit</button>
				</form>
			</div>
		</div>

<?php require 'partials/footer.php'; ?>
