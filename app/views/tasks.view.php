<?php require 'partials/head.php'; ?>

		<!-- Perform the task details listing -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<ol>
					<?php foreach($tasks as $task): ?>
						<?php if ($task->completed): ?>
							<li><?= ucfirst($task->description); ?> &nbsp; &#9989;</li>
						<?php else: ?>
							<li><?= ucfirst($task->description); ?></li>
						<?php endif ?>
					<?php endforeach ?>
				</ol>
			</div>
		</div>
		<!-- form submission -->
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<h2>Form Practising (Tasks creation)</h2>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-2 col-md-4 col-sm-6 col-xs-12">
				<form method="POST" action="tasks">
					<label for="name" class="control-label">Username</label>
					<input type="text" name="name" class="form-control" required>
					<br>
					<label for="title" class="control-label">Task title</label>
					<input type="text" name="task_title" class="form-control" required>
					<br>
					<label for="description" class="control-label">Task description</label>
					<input type="text" name="task_description" class="form-control" required>
					<br>
					<label for="completed" class="control-label">Task status</label>
					<input type="number" name="task_completed" class="form-control" placeholder="1 or 0" 
					maxlength="1" required>
					<br>
					<button type="submit" class="btn-primary btn-lg">Submit</button>
				</form>
			</div>
		</div>

<?php require 'partials/footer.php'; ?>