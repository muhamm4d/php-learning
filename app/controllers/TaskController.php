<?php

namespace App\Controllers;
use App\Core\App;

class TaskController
{
	public function index()
	{
		$tasks = App::get('database')->selectAll('todos'); // option 2
		return view('tasks', compact('tasks'));
	}

	public function store()
	{
		if (!empty($_POST)) 
		{
			$name = $_POST['name'];
			$title = $_POST['task_title'];
			$description = $_POST['task_description'];
			$completed = $_POST['task_completed'];

			$is_completed = $completed ? true : false;

			$insert_task = App::get('database')->createTask('todos', 
				$name, $title, $description, $completed);

			/*$insert_task = $app['database']->createTask('todos', [
				'username' => $name, 
				'title' => $title, 
				'description' => $description, 
				'completed' => $is_completed
			]);*/

			if ($insert_task)
				return view('success');	// return redirect('success');
			else
				return view('failed');
			
			/*$name = $_POST['name'];
			$age = $_POST['age'];

			$app['database']->insert('users', [
				'name' => $name,
				'age' => $age
			]);*/

		}
	}
}