<?php

//$tasks = $query->selectAll('todos', 'Task'); // option 1
//$tasks = $app['database']->selectAll('todos'); // no more using DI container option 2
$tasks = App::get('database')->selectAll('todos'); // option 2

require 'views/index.view.php';
