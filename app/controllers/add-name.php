<?php

if (!empty($_POST)) 
{
	$name = $_POST['name'];
	$title = $_POST['task_title'];
	$description = $_POST['task_description'];
	$completed = $_POST['task_completed'];

	$is_completed = $completed ? true : false;

	$insert_task = App::get('database')->createTask('todos', $name, $title, $description, $completed);

	/*$insert_task = $app['database']->createTask('todos', [
		'username' => $name, 
		'title' => $title, 
		'description' => $description, 
		'completed' => $is_completed
	]);*/

	if ($insert_task)
		require 'views/success.view.php';
	else
		require 'views/failed.view.php';
	
	/*$name = $_POST['name'];
	$age = $_POST['age'];

	$app['database']->insert('users', [
		'name' => $name,
		'age' => $age
	]);*/

}