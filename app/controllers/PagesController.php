<?php

namespace App\Controllers;
use App\Core\App;

class PagesController
{
	public function home()
	{
		$tasks = App::get('database')->selectAll('todos'); // option 2

		// require 'views/index.view.php';
		return view('index', ['tasks' => $tasks]); // or use compact('tasks')
	}

	public function about()
	{
		return view('about');
	}

	public function aboutCulture()
	{
		return view('about-culture');
	}

	public function contact()
	{
		return view('contact');
	}

	public function addTask()
	{
		if (!empty($_POST)) 
		{
			$name = $_POST['name'];
			$title = $_POST['task_title'];
			$description = $_POST['task_description'];
			$completed = $_POST['task_completed'];

			$is_completed = $completed ? true : false;

			$insert_task = App::get('database')->createTask('todos', 
				$name, $title, $description, $completed);

			/*$insert_task = $app['database']->createTask('todos', [
				'username' => $name, 
				'title' => $title, 
				'description' => $description, 
				'completed' => $is_completed
			]);*/

			if ($insert_task)
				return view('success');
			else
				return view('failed');
			
			/*$name = $_POST['name'];
			$age = $_POST['age'];

			$app['database']->insert('users', [
				'name' => $name,
				'age' => $age
			]);*/

		}
	}
}