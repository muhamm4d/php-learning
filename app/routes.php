<?php

/*$router->define([
	'' => 'controllers/index.php',	
	'about' => 'controllers/about.php',	
	'about/culture' => 'controllers/about-culture.php',	
	'contact' => 'controllers/contact.php',
	'names' => 'controllers/add-name.php' // only for post items	
]);*/

// other frameworks : $router->define('reference', 'controllers/reference.php');

/*$router->get('', 'controllers/index.php');
$router->get('about', 'controllers/about.php');
$router->get('about/culture', 'controllers/about-culture.php');
$router->get('contact', 'controllers/contact.php');
$router->post('names', 'controllers/add-name.php');*/

$router->get('', 'PagesController@home');
$router->get('about', 'PagesController@about');
$router->get('about/culture', 'PagesController@aboutCulture');
$router->get('contact', 'PagesController@contact');
// $router->post('names', 'PagesController@addTask');

$router->get('tasks', 'TaskController@index');
$router->post('tasks', 'TaskController@store');