<?php

/**
* Responsible for building queries
*/
class QueryBuilder
{
	protected $pdo;

	public function __construct($pdo)
	{
		$this->pdo = $pdo;
	}

	//public function selectAll($table, $useClass) //opt 1
	public function selectAll($table) //opt 2
	{
		$statement = $this->pdo->prepare("SELECT * FROM {$table}");
		$statement->execute();
		//return $statement->fetchAll(PDO::FETCH_CLASS, $useClass); //opt 1
		return $statement->fetchAll(PDO::FETCH_CLASS); //opt 2
		// can adjust so it uses our own class PDO::FETCH_OBJ
	}

	public function createTask($table, $name, $title, $description, $completed)
	{
		
		$statement = $this->pdo->prepare("
			INSERT INTO {$table}(username, title, description, completed)
			VALUES(:username, :title, :description, :completed)
		");

		$statement->bindParam(':username', $name);
		$statement->bindParam(':title', $title);
		$statement->bindParam(':description', $description);
		$statement->bindParam(':completed', $completed);

		$statement->execute();
		return true;

	}

	public function insert($table, $parameters)
	{
		$sql = sprintf(
			'insert into %s(%s %s) values(%s %s)',
			$table, 
			implode(' ,', array_keys($parameters)),
			':' . implode(', :', array_keys($parameters))
		);

		try {
			$statement = $this->pdo->prepare($sql);
			$statement->execute($parameters);
		} catch (Exception $e) {
			die('Whoops, something went really wrong');
		}
	}
}