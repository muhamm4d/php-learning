<?php

use App\Core\App;

App::bind('config', require 'config.php');

App::bind('database', new QueryBuilder(
	Connection::make(App::get('config')['database'])
));

function view($name, $data = [])
{
	extract($data);
	return require "app/views/{$name}.view.php";
}

function redirect($uri_path)
{
	header("Location: /{$uri_path}");
}
/*
$app = [];
$app['config'] = require 'config.php';*/

/*require 'core/Request.php';
require 'core/Router.php';
require 'core/database/Connection.php';
require 'core/database/QueryBuilder.php';
=> commented cause now we use autoload from composer and u only need
to include that in the entry point of ur application */

/*$app['database'] = new QueryBuilder(
	Connection::make($app['config']['database'])
);*/